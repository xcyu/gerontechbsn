const tm_multiplier = 5
const NODE= '201-1'
var now = new Date(Date.now())
//const NODE= '00-AE-FA-A7-C6-04'
function rand() {
    return Math.random();
}

var layout_res = {
  title: 'Real-time Respiration Rate',
  xaxis: {
    title:  'time(s)',
    titlefont: {
      size: 16
    }
  },
  yaxis: {
    title:  'rate(bpm)',
    titlefont: {
      size: 16
    }
  }
}
var layout_rate = {
	font: {
		color:'black',
	},
  title: '心電圖',
  titlefont: {
	  color: 'FFD84D',
  },
	
  xaxis: {
//	  visible: false,
//    title:  '秒',
	 showticklabels: false,
    titlefont: {
      size: 18
    },
//	  rangemode: 'tozero',
	  rangemode: 'normal',
	  fixedrange: true, 
  },
  yaxis: {
//	  visible: false,
//    title:  'bpm',
	  showticklabels: false,
    titlefont: {
      size: 18
    },
	  fixedrange: true, 
  },
  autosize: true,
	
	margin: {
		l: 30,
		r:30,
		t:40,
		b:20,
		pad:20,
	},
  paper_bgcolor: 'rgba(0,0,0,0)',
  plot_bgcolor: 'rgba(0,0,0,0)',
	colorway : ['#5dc6ff'],
	
}
var layout_ss= {
    title:'Real-time movement measure',
    yaxis: {title:'amplitude'}
};
var layout_sv= {
    title:'Real-time stroke volume',
    yaxis: {title:'volume(ul)'}
};
var layout_hv= {
    title:'Heart rate variance',
    yaxis: {title:'variance'}
};

var layout_oc={
    title:'Bed occupency detection',
    yaxis:{title:'bool',
}};

//Plotly.plot('res', [{
    //x: [now],
    //y: [0],
    //name: 'respiration rate',
    //line: {color: 'orange'}
//}],layout_res);


Plotly.plot('rate', [{
    x: [now],
    y: [0],
    name: 'heart rate',
}],layout_rate,{
	responsive: true,
	displayModeBar: false,
	
	
	
	
});

//Plotly.plot('ss', [{
    //x: [now],
    //y: [0],
    //name: 'signal strength',
    //fill: 'tozeroy',
    //type: 'scatter',
    //mode: 'lines',
    //line: {color: 'green'}
//}, ],layout_ss);

//Plotly.plot('sv', [{
    //x: [now],
    //y: [0],
    //name: 'signal strength',
    //fill: 'tozeroy',
    //type: 'scatter',
    //mode: 'lines',
    //line: {color: 'blue'}
//}, ],layout_sv);

//Plotly.plot('hv', [{
    //x: [now],
    //y: [0],
    //name: 'signal strength',
    //fill: 'tozeroy',
    //type: 'scatter',
    //mode: 'lines',
    //line: {color: 'orange'}
//}, ],layout_hv);

//Plotly.plot('oc', [{
    //x: [now],
    //y: [0],
    //name: 'oc1',
    //type: 'tozeroy',
    //mode: 'lines',
//}, {
    //x: [now],
    //y: [0],
    //name: 'oc2',
    //fill: 'tozeroy',
    //type: 'scatter',
    //mode: 'lines',
//},{
    //x: [now],
    //y: [0],
    //name: 'oc3',
    //fill: 'tozeroy',
    //type: 'scatter',
    //mode: 'lines',
//}],layout_oc);

var cnt = 0;

var interval = setInterval(function() {
    $.ajax({
        type: 'GET',
        url: '/data/pull',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify({'date':'20170924'}),
        context: this,
        success: function(result) {
            result = result[NODE]
            var hr = result['heartRate']
			
			//update bpm in html
			document.getElementById('bpm').innerHTML = hr + '<sub class="hrnumsup">bpm</sub>';
			
            var rr = result['respirationRate']
            var id = result['node']
            var time = [new Date(Date.now())]
            //var time = [new Date(result['time'][0])] unless we need device clock
            var ss = result['sigStrength']
            var sv = result['strokeVolume']
            var hv = result['heartRateVariance']
            var oc1 = result['oc1']
            var oc2 = result['oc2']
            var oc3 = result['oc3']

            //Plotly.extendTraces('res', {
                //x: [time],
                //y: [rr]
            //}, [0], 10); 
            Plotly.extendTraces('rate', {
                x: [time],
                y: [hr]
            }, [0], 10);
            //Plotly.extendTraces('ss', {
                //x: [time],
                //y: [ss]
            //}, [0], 10);
            //Plotly.extendTraces('sv', {
                //x: [time],
                //y: [sv]
            //}, [0], 10);
            //Plotly.extendTraces('hv', {
                //x: [time],
                //y: [hv]
            //}, [0], 10);
            ////Plotly.relayout('ss', layout);
            //Plotly.extendTraces('oc', {
                //x: [time,time,time],
                //y: [oc1, oc2, oc3]
            //}, [0, 1, 2], 10);
        },
        error: function() {
            $("#text").html("error!");
        }
    });

    cnt++;


    //if(cnt === 100) clearInterval(interval);
}, 1000*tm_multiplier);
